# Copyright 2008, 2009 David Leverton <dleverton@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require multibuild flag-o-matic game alternatives

MY_PV=$(ever delete_all)
MY_PNV=${PN}-${MY_PV}

SUMMARY="Classic single-player dungeon exploration game"
HOMEPAGE="http://www.nethack.org/"
DOWNLOADS="mirror://sourceforge/${PN}/${MY_PNV}-src.tgz"

# from http://www.nethack.org/common/info.html#Basic
DESCRIPTION="NetHack is a single player dungeon exploration game that
runs on a wide variety of computer systems, with a variety of
graphical and text interfaces all using the same game engine. Unlike
many other Dungeons & Dragons-inspired games, the emphasis in NetHack
is on discovering the detail of the dungeon and not simply killing
everything in sight - in fact, killing everything in sight is a good
way to die quickly. Each game presents a different landscape - the
random number generator provides an essentially unlimited number of
variations of the dungeon and its denizens to be discovered by the
player in one of a number of characters: you can pick your race, your
role, and your gender."

BUGS_TO="dleverton@exherbo.org"
REMOTE_IDS="freecode:nethack"
UPSTREAM_DOCUMENTATION="http://www.nethack.org/v${MY_PV}/Guidebook.html"
UPSTREAM_RELEASE_NOTES="http://www.nethack.org/v${MY_PV}/release.html"

LICENCES="NetHack"
SLOT="$(ever range 1-2)"
# Has Qt 3.x, GNOME 1.x and KDE 2.x support, but we don't want it
MYOPTIONS="xaw"

DEPENDENCIES="
     build:
         sys-apps/groff
         sys-apps/util-linux [[
             note = [ uses /usr/bin/col to build the Guidebook ]
         ]]
         sys-devel/bison
         sys-devel/flex
     run:
         app-arch/bzip2
     build+run:
         sys-libs/ncurses

     xaw? (
         build+run:
             x11-libs/libXaw
             x11-libs/libXt
             x11-libs/libXpm
             x11-libs/libX11
         build:
             fonts/encodings
             x11-apps/bdftopcf
             x11-apps/mkfontdir
             x11-proto/xorgproto
     )
"

nethack_src_prepare() {
    default
    sh sys/unix/setup.sh || die "setup.sh failed"
}

nethack_define() {
    sed -i -e 's,\(/\*[ \t]*\)\?\(#[ \t]*define[ \t]\+'${2}'\>\)\([ \t]\+"[^"]*"\)\?\([ \t]*\*/\)\?,\2 '${3:+\"${3}\"}',' include/${1}.h || die "sed failed"
}

nethack_undef() {
    sed -i -e 's,#[ \t]*define[ \t]\+'${2}'\>\([ \t]\+"[^"]*"\)\?,/* & */,' include/${1}.h || die "sed failed"
}

nethack_src_configure() {
    option xaw && nethack_define config X11_GRAPHICS
    option xaw && nethack_define config USE_XPM

    nethack_define config COMPRESS /bin/bzip2
    nethack_define config COMPRESS_EXTENSION .bz2

    nethack_define config   HACKDIR             /usr/$(exhost --target)/lib/nethack-${PV}
    nethack_define unixconf VAR_PLAYGROUND      /var/lib/games/nethack
    nethack_define unixconf VAR_PLAYGROUND_SLOT /var/lib/games/nethack/${SLOT}

    nethack_define config VISION_TABLES
    nethack_define config SCORE_ON_BOTL

    nethack_define unixconf LINUX
    nethack_define unixconf TIMED_DELAY

    nethack_undef unixconf DEF_MAILREADER

    local var
    for var in SRC OBJ LIB; do
        sed -i -e "s,WIN${var} = .*,WIN${var} = $(
            for sys in TTY $(option xaw && echo X11); do
                echo -n ' $(WIN'${sys}${var}')'
            done)", src/Makefile || die "sed failed"
    done

    local data=( )
    # install NetHack.ad and nethack.rc in src_install
    option xaw && data+=( pet_mark.xbm )
    option xaw && data+=( rip.xpm )

    if option xaw; then
        sed -i -e 's, tile\.o, ,' -e 's,WINOBJ =,& tile.o,' src/Makefile || die "sed failed"
        data+=( x11tiles )
    fi

    sed -i -e "s,VARDATND =,& ${data[*]}," Makefile || die "sed failed"

    append-flags -I../include -I. -Dindex=index -Drindex=rindex
}

nethack_src_compile() {
    emake all \
        CC="${CC}" CFLAGS="${CFLAGS}" \
        LINK="${CC}" \
        LDFLAGS="${LDFLAGS}" WINTTYLIB=-lncurses \
        WINX11LIB="-lXaw -lXt -lXpm -lX11" \
        YACC="bison -y" LEX=flex

    if option xaw; then
        bdftopcf win/X11/ibm.bdf -o ibm.pcf || die "bdftopcf failed"
        bdftopcf win/X11/nh10.bdf -o nh10.pcf || die "bdftopcf failed"
    fi
}

nethack_src_install() {
    # fix up the ownership/permissions manually
    emake install \
        GAMEUID=root GAMEGRP=root GAMEPERM=0755 \
        GAMEDIR="${IMAGE}"/usr/$(exhost --target)/lib/nethack-${PV} \
        VARDIR="${IMAGE}"/var/lib/games/nethack \
        SHELLDIR="${IMAGE}"/usr/$(exhost --target)/bin/
    dodoc README Porting doc/{Guidebook,fixes*}

    if option xaw; then
        dodoc win/X11/nethack.rc
        insinto /etc/X11/app-defaults
        newins win/X11/NetHack.ad NetHack

        insinto /usr/$(exhost --target)/lib/nethack-${PV}/fonts
        doins ibm.pcf nh10.pcf

        mkfontdir \
            -e /usr/share/fonts/X11/encodings \
            -e /usr/share/fonts/X11/encodings/large \
            -- "${IMAGE}"/usr/$(exhost --target)/lib/nethack-${PV}/fonts || die "mkfontdir failed"
    fi

    sed -i \
        -e "s:${IMAGE}::" \
        -e "s,HACK=\$HACKDIR/nethack,HACK=/usr/libexec/nethack-${PV}," \
        -e '/Since Nethack\.ad/,/export XUSERFILESEARCHPATH/d' \
        "${IMAGE}"/usr/$(exhost --target)/bin/nethack || die "sed failed"
    doman doc/nethack.6
    alternatives_for nethack ${SLOT} ${SLOT} /usr/$(exhost --target)/bin/nethack nethack-${PV}
    alternatives_for nethack ${SLOT} ${SLOT} /usr/share/man/man6/nethack.6 nethack-${PV}.6

    dodir /usr/$(exhost --target)/libexec
    mv "${IMAGE}"/usr/$(exhost --target)/{lib/nethack-${PV}/nethack,libexec/nethack-${PV}} || die "mv failed"
    edo chown wizard:games "${IMAGE}"/usr/$(exhost --target)/libexec/nethack-${PV}
    edo chmod ug+s "${IMAGE}"/usr/$(exhost --target)/libexec/nethack-${PV}

    mv "${IMAGE}"/usr/$(exhost --target)/{lib/nethack-${PV}/recover,bin} || die "mv failed"
    doman doc/recover.6
    alternatives_for nethack ${SLOT} ${SLOT} /usr/$(exhost --target)/bin/recover recover-${PV}
    alternatives_for nethack ${SLOT} ${SLOT} /usr/share/man/man6/recover.6 recover-${PV}.6

    rmdir "${IMAGE}"/var/lib/games/nethack/save || die "rmdir failed"
    keepdir /var/lib/games/nethack{,/${SLOT}/save}
    touch "${IMAGE}"/var/lib/games/nethack/paniclog || die "touch failed"
    preserve_scores "${IMAGE}"/var/lib/games/nethack/*
    dovarlibgames -R
    # hide bones
    edo chmod o-r "${IMAGE}"/var/lib/games/nethack/${SLOT}
}

nethack_pkg_postinst() {
    game_pkg_postinst
    alternatives_pkg_postinst
}

export_exlib_phases src_prepare src_configure src_compile src_install pkg_postinst

