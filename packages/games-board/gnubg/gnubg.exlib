# Copyright 2010 Anders Marchsteiner <alm.anma@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.bz2 ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.11 ] ]

export_exlib_phases src_install

SUMMARY="GNU Backgammon"
DESCRIPTION="
GNU Backgammon (gnubg) is for playing and analysing backgammon positions, games and matches. It's
based on a neural network. In the past twelve months it has made enormous progress. It currently
plays at about the level of a championship fight tournament player.
"
DOWNLOADS="http://www.gnubg.org/media/sources/${PN}-source-SNAPSHOT-${PV}.tar.gz"

LICENCES="GPL-2 GPL-3"
SLOT="0"
MYOPTIONS="
    board3d [[ description = [ Enable 3D board view ] requires = [ gtk ] ]]
    gtk [[
        description = [ Provide a GTK frontend for playing. A CLI frontend is provided if GTK is not compiled in ]
    ]]
    python
    sqlite
"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2
        media-libs/freetype:2
        board3d? ( x11-libs/gtkglext )
        gtk? ( x11-libs/gtk+ )
        python? ( dev-lang/python )
        sqlite? ( dev-db/sqlite )
"

BUGS_TO="alm.anma@gmail.com"

WORK="${WORKBASE}/${PN}"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    board3d
    gtk
    python
    sqlite
)

AT_M4DIR=( "m4" )
AM_OPTS="-Wno-portability"

gnubg_src_install() {
    default
    insinto /usr/share/applications
    hereins gnubg.desktop <<EOF
[Desktop Entry]
Type=Application
Name=GNU Backgammon
Icon=gnubg
Exec=gnubg
Categories=Game;BoardGame;
EOF
}

