# Copyright 2009 David Leverton <dleverton@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.11 ] ]

SUMMARY="Go board for X11"
HOMEPAGE="http://cgoban1.sourceforge.net/"
DOWNLOADS="mirror://sourceforge/cgoban1/${PNV}.tar.gz"

# from ${HOMEPAGE}
DESCRIPTION="CGoban1 (\"Complete Goban Mark 1\") provides a large set
of go-related services for Unix and X11. A list of the functions that
it provides:

 * Play go against another player. When playing a game locally, you
   have a choice of rule sets and time controls. I made a strong
   attempt at implementing the rule sets as precisely as possible:
    o Chinese rules have a true Superko rule.
    o Japanese rules correctly recognize seki and award no points.
    o Japanese rules let you locally resolve disputes at the end of
      the game.

 * Edit and view SGF files. CGoban provides full functionality for
   editing and viewing SGF files.

 * Connect to a go server over the internet. CGoban can connect to
   NNGS or IGS and gives a convienent graphical user interface to the
   server.

 * Act as a bridge to go modem protocol. Go modem protocol has become
   a standard way for computer go programs to communicate. CGoban can
   let you connect to devices (such as modems) speaking go modem
   protocol, act as a graphical board for programs that speak go modem
   protocol out of stdin/stdout, or connect programs to one of the
   internet go servers."

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        x11-libs/libX11

    suggestion:
        games-board/gnugo     [[ description = [ AI opponent to play against ] ]]

        x11-apps/xwininfo     [[ description = [ Needed for the grab_cgoban script ] ]]
        x11-apps/xwd          [[ description = [ Needed for the grab_cgoban script ] ]]
        media-gfx/ImageMagick [[ description = [ Used by the grab_cgoban script if available ] ]]
        media-gfx/netpbm      [[ description = [ Used by the grab_cgoban script if available and ImageMagick is not present ] ]]
"

BUGS_TO="dleverton@exherbo.org"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p1 "${FILES}"/cgoban-1.9.14-CFLAGS.patch
)

src_install() {
    default
    dodoc seigen-minoru.sgf

    insinto /usr/share/pixmaps
    doins cgoban_icon.png
    insinto /usr/share/applications
    hereins cgoban.desktop <<EOF
[Desktop Entry]
Type=Application
Name=CGoban
GenericName=Go Board
Icon=cgoban_icon
Exec=cgoban
Categories=Game;BoardGame;
EOF
}

