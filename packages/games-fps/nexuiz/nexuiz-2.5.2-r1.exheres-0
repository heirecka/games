# Copyright 2010 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=zip pv=${PV//.} ]

SUMMARY="Nexuiz is a 3D deathmatch game"
HOMEPAGE="http://alientrap.org/nexuiz/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    sdl [[ description = [ Builds the nexuiz binary using SDL ( nexuiz-sdl ) ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-arch/unzip
    build+run:
        sys-sound/alsa-lib
        x11-dri/mesa
        x11-libs/libXxf86dga
        x11-libs/libXxf86vm
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sdl? ( media-libs/SDL:0[X] )
    recommendation:
        media-libs/libvorbis [[ description = [ For ogg sound data support (needed for sound playback) ] ]]
        net-misc/curl [[ description = [ Download remote data for network play ] ]]
    suggestion:
        media-libs/libmodplug [[ description = [ For modplug sound data support (needed for some mappacks) ] ]]
        media-libs/libpng [[ description = [ For png image support (needed for some mappacks) ] ]]
        media-libs/libtheora [[ description = [ For internal video capturing support ] ]]
"

WORK="${WORKBASE}/darkplaces"

src_unpack() {
    default

    unpack ./Nexuiz/sources/enginesource*
}

src_compile() {
    targets=( "cl-nexuiz" "sv-nexuiz" $(option sdl && echo "sdl-nexuiz") )

    emake \
        CC="${CC}" \
        DP_FS_BASEDIR=/usr/share/games/${PN} \
        DP_LINK_TO_LIBJPEG=1 \
        OPTIM_RELEASE="${CFLAGS} ${LDFLAGS}" \
        STRIP=true \
        ${targets[*]}
}

src_install() {
    dobin nexuiz-glx
    dobin nexuiz-dedicated
    option sdl && dobin nexuiz-sdl

    insinto /usr/share/games/${PN}

    doins -r "${WORKBASE}"/Nexuiz/{data,havoc}
}

