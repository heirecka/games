# Copyright 2014 Konstantinn Bonnet <qu7uux@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'ioquake3-git' from Archlinux, which is:
#   Maintainer: Slash <demodevil5[at]yahoo[dot]com>
#   Contributors: OttoA (AUR), hoschi (AUR), samlt (AUR), andreyv (AUR)

require github [ user=ioquake pn=ioq3 ]

export_exlib_phases src_prepare src_configure src_install pkg_postinst pkg_postrm

SUMMARY="Free software (GPLv2+) first person shooter engine, used to play Quake 3: Arena and others"
DESCRIPTION="
ioquake3 is a modern FPS engine based on id software's Quake 3: Arena engine. It incorporates many
fixes and additions, resolving security flaws, adding full x86_64 support, IPv6 networking and
others.
"
HOMEPAGE="http://${PN}.org"

DOWNLOADS="
http://ftp.gwdg.de/pub/misc/ftp.idsoftware.com/idstuff/quake3/linux/linuxq3apoint-1.32b-3.x86.run
    -> ioquake3-data.run
"

LICENCES="GPL-2 Quake3-EULA"
SLOT="0"

MYOPTIONS="
    mumble [[
        description = [ Use Mumble instead of the built-in Speex-based VoIP ]
    ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-arch/unzip
    build+run:
        media-libs/SDL:=[>=1.2]
        media-libs/libogg
        media-libs/openal
        media-libs/opusfile
        media-libs/speex
        media-libs/speexdsp
        net-misc/curl
        sys-libs/zlib
        x11-dri/glu
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXext
        mumble? ( voip/mumble )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( NOTTODO id-readme.txt md4-readme.txt opengl2-readme.txt voip-readme.txt )

DEFAULT_SRC_COMPILE_PARAMS=(
    BUILD_CLIENT=1
    BUILD_RENDERER_OPENGL2=1    # build newer renderer
    BUILD_SERVER=1
    BUILD_STANDALONE=0          # do not build binaries suited for stand-alone games
    BUILD_BASEGAME=0            # the following four variables control building of non-essential
                                # stuff: Quake VMs, libraries, building for 3rd party mods.
    BUILD_GAME_QVM=0
    BUILD_GAME_SO=0
    BUILD_MISSIONPACK=0
    COPYBINDIR="${IMAGE}"/usr/$(exhost --target)/bin
    DEFAULT_BASEDIR=/usr/share/games/${PN}
    FULLBINEXT=""
    GENERATE_DEPENDENCIES=0     # do not add -MMD for the preprocessor
    NO_STRIP=1                  # let us do the stripping
    OPTIMIZE=""                 # do add more optimization CFLAGS
    OPTIMIZEVM=""               # see above
    USE_CURL_DLOPEN=0           # *_DLOPEN: do not link at runtime
    USE_INTERNAL_JPEG=0
    USE_INTERNAL_OGG=0
    USE_INTERNAL_OPUS=0
    USE_INTERNAL_SPEEX=0
    USE_INTERNAL_ZLIB=0
    USE_LOCAL_HEADERS=0         # do not use headers local to ioq3 instead of system ones
    USE_OPENAL_DLOPEN=0
    USE_RENDERER_DLOPEN=0
    V=1                         # show cc command line when building
    release                     # only build release target, do not build debug target
)

ioquake3_src_prepare() {
    # extract data files
    mkdir "${WORK}"/ioquake3-data
    [[ -n ${ARCHIVES} ]] && edo sh "${FETCHEDDIR}"/${ARCHIVES} --tar xf -C "${WORK}"/ioquake3-data

    # fix pkg-config
    edo sed -e "s;pkg-config;${PKG_CONFIG};" -i "${WORK}"/Makefile
}

ioquake3_src_configure() {
    # Makefile.local is included by Makefile for both release *and* copyfiles target.
    # pkg-config is needed for opus, otherwise opusfile.h cannot be found. Upstream does not support
    # any autotools.
    edo echo "
    CFLAGS+="$(${PKG_CONFIG} --cflags opusfile)"
    USE_MUMBLE=$(option mumble 1 0)
    " >> Makefile.local
}

ioquake3_src_install() {
    edo mkdir -p -m 0755 "${IMAGE}"/usr/$(exhost --target)/bin
    edo mkdir -p -m 0755 "${IMAGE}"/usr/share/games/${PN}

    # copy pak*.pk3 files from extracted archive
    insinto /usr/share/games/${PN}
    doins -r ${PN}-data/{baseq3,missionpack}

    emake "${DEFAULT_SRC_COMPILE_PARAMS[@]}" copyfiles
    emagicdocs

    edo install -m755 -d "${IMAGE}"/usr/share/applications/
    edo install -m644 misc/setup/${PN}.desktop "${IMAGE}"/usr/share/applications
    edo install -m755 -d "${IMAGE}"/usr/share/icons/hicolor/scalable/apps/
    edo install -m644 misc/quake3.svg "${IMAGE}"/usr/share/icons/hicolor/scalable/apps
    edo install -m755 -d "${IMAGE}"/usr/share/pixmaps/
    edo install -m755 misc/quake3.png "${IMAGE}"/usr/share/pixmaps
}

ioquake3_pkg_postinst() {
    default

    # NOTE: instructions on how to play with the pak0.pk3 from a demo release are present on the github
    # page, but it's not something that is much maintained by upstream.

    ewarn "You must still manually install pak0.pk3 from a genuine Quake 3 installation or CD!"
    ewarn "Please place it in /usr/share/games/${PN}/baseq3/."
}

ioquake3_pkg_postrm() {
    default

    ewarn "Please remove pak0.pk3 from /usr/share/games/${PN}/baseq3/ ."
}

BUGS_TO="qu7uux@gmail.com"

UPSTREAM_DOCUMENTATION="http://wiki.${PN}.org/"

