# Copyright 2009 Ingmar Vanhassel
# Copyright 2012 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=smclone ]

MUSIC_ZIP=SMC_Music_4.1_high.zip

SUMMARY="Secret Maryo Chronicles"
DESCRIPTION="
Secret Maryo Chronicles is a two-dimensional platform game with a style similar
to classic side-scroller games. It features an in-game editor, many levels, and
many power-ups.
"
HOMEPAGE="http://www.secretmaryo.org/"
DOWNLOADS+=" mirror://sourceforge/smclone/${MUSIC_ZIP}"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-arch/unzip
        virtual/pkg-config
    build+run:
        dev-games/cegui[>=0.5.0&<0.7][devil][opengl]
        dev-libs/boost
        dev-libs/pcre
        media-libs/SDL:0[>=1.2.10]
        media-libs/SDL_image:1
        media-libs/SDL_mixer
        media-libs/SDL_ttf:0
        media-libs/libpng:=
        x11-dri/glu
        x11-dri/mesa[>=9]
        x11-libs/libX11
"

DEFAULT_SRC_PREPARE_PATCHES=( -p0 "${FILES}"/bdeda63e45a738c016d35ea2c1554bd0426d6ce3.patch )

src_unpack() {
    unpack ${PNV}.tar.bz2
    edo cd "${WORK}"
    unpack ${MUSIC_ZIP}
}

src_prepare() {
    default

    # boost filesystem version 2 -> 3
    edo sed -e 's/leaf()/filename().string()/g' \
            -e 's/, fs::native//g' \
            -i src/overworld/world_manager.cpp \
            -i src/core/filesystem/filesystem.cpp \
            -i src/video/video.cpp
}

src_install() {
    default
    insinto /usr/share/applications
    doins makefiles/unix/${PN}.desktop
    insinto /usr/share/pixmaps
    doins makefiles/unix/${PN}.xpm
}
