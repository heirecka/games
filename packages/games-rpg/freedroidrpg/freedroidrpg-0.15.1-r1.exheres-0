# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require lua [ whitelist=5.1 multibuild=false ]

SUMMARY="FreedroidPRG is a mature science fiction role playing game set in the future"
DESCRIPTION="
FreedroidRPG is an isometric RPG game inspired by Diablo and Fallout. There is
a war, and we are losing it. No one really knows why the bots started killing
us. Most don't even want to know. The human race is reduced to little more than
a few isolated groups of people trying to survive. Somewhere in a place known
as Temple Wood, a hero is about to wake up from a long sleep...
"
HOMEPAGE="http://freedroid.sf.net/"
DOWNLOADS="mirror://sourceforge/${PN%rpg}/${PN%rpg}RPG-${PV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/SDL:0[>=1.2.3]
        media-libs/SDL_gfx[>=0.0.21]
        media-libs/SDL_image:1
        media-libs/SDL_mixer[>=1.2.1]
        x11-dri/glu
        x11-dri/mesa
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

AT_M4DIR=( m4 )

src_prepare() {
    # It only works with lua:5.1, but wrongly accepts newer SLOTs as well.
    edo sed -e "/PKG_CHECK_MODULES(\[LUA\], \[/s:lua:lua-5.1:" -i m4/lua.m4

    autotools_src_prepare
}

