# Copyright 2010, 2011 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Distributed under the terms of the GNU General Public License v2

MY_PV=build${PV}

require launchpad [ branch=${MY_PV} pv=build-${PV} pnv=${PN}-${MY_PV}-src suffix=tar.bz2 ] \
        cmake [ api=2 ] lua [ whitelist='5.1' multibuild=false ]

SUMMARY="Widelands is an real-time strategy game similar to the Settlers II"
HOMEPAGE="http://wl.widelands.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/python:*[>=2.3&<3]
    build+run:
        dev-libs/boost[>=1.48.0]
        media-libs/SDL:0[>=1.2.8]
        media-libs/SDL_gfx
        media-libs/SDL_image:1
        media-libs/SDL_mixer[ogg][>=1.2.6]
        media-libs/SDL_net
        media-libs/SDL_ttf:0[>=2.0.0]
        media-libs/glew
        media-libs/libpng:=
        sys-devel/gettext
        sys-libs/zlib
        x11-dri/mesa
"

# -DCMAKE_BUILD_TYPE: the files are only installed for Release and Debug
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING='Release'
    -DWL_INSTALL_PREFIX:PATH=/usr
    -DWL_INSTALL_BINDIR:PATH=bin
    -DWL_INSTALL_DATADIR:PATH=/usr/share/games/${PN}
    -DWL_UNIT_TESTS=On
)

CMAKE_SOURCE=${WORKBASE}/${PN}-${MY_PV}-src
DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/0001-BOOST_MESSAGGE-was-deprecated-with-1.35-and-removed-.patch )

src_prepare() {
    cmake_src_prepare

    # -DCMAKE_CXX_FLAGS_RELEASE above doesn't work due to CMakeLists hardcoding.
    edo sed -i -e "s|-O3 -DNDEBUG|-DNDEBUG|" CMakeLists.txt
}

src_install() {
    cmake_src_install

    edo rmdir "${IMAGE}"/usr/share/games/widelands/maps/{'Swamp Monks.wmf','Desert Tournament.wmf'}/scripting
}

