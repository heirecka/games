# Copyright 2010-2012 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Copyright 2014 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge flag-o-matic

if ever is_scm ; then
    SCM_REPOSITORY="git://git.code.sf.net/p/ufoai/code"
    require scm-git
else
    DOWNLOADS="
        mirror://sourceforge/${PN}/${PNV}-source.tar.bz2
        mirror://sourceforge/${PN}/${PNV}-data.tar
    "
fi

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="A squad-based tactical strategy game in the tradition of the old X-COM PC games."
HOMEPAGE="http://ufoai.org"

LICENCES="GPL-2"
# Actually, things are way more complicated. In its entirety, UFO:AI is licenced
# under the GPL-2 but there are 7028 copyright notices, ultimately boiling down
# to these:
# Creative Commons Attribution 2.5
# Creative Commons Attribution 3.0 Unported
# Creative Commons Attribution-Share Alike 2.0
# Creative Commons Attribution-Share Alike 3.0
# Creative Commons Attribution-Share Alike 3.0 Unported
# GNU Free Documentation License
# GNU General Public License 3.0
# MIT/X11
# Open Font License
# Public Domain
# umefont
# cf. LICENCES

SLOT="0"
MYOPTIONS="
    debug
    ( platform: amd64 )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

if ! ever is_scm ; then
    MYOPTIONS+="
        editor [[ description = [ Tools to make custom data files (maps, models, etc.) ] ]]
    "
fi

# The tests are started despite --disable-testall, fail miserably but non-fatally.
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        media-libs/SDL:0[>=1.2.10][X]
        media-libs/SDL_image:1[>=1.2.10]
        media-libs/SDL_mixer[>=1.2.7][ogg]
        media-libs/SDL_ttf:0[>=2.0.7]
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libtheora
        media-libs/libvorbis
        media-libs/xvid
        net-misc/curl
        x11-dri/mesa
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6.2] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

if ever is_scm ; then
    DEPENDENCIES+="
        build+run:
            dev-lang/lua[>=5.1]
            dev-libs/atk
            dev-libs/glib:2
            dev-libs/libxml2
            gnome-desktop/gtksourceview:2
            media-libs/fontconfig
            media-libs/freetype:2
            media-libs/openal
            x11-dri/glu
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:2
            x11-libs/gtkglext
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libX11
            x11-libs/libXmu
            x11-libs/libXt
            x11-libs/pango
            x11-libs/pangox-compat
    "
else
    DEPENDENCIES+="
        build+run:
            editor? (
                dev-libs/glib:2
                dev-libs/libxml2
                gnome-desktop/gtksourceview:2
                media-libs/openal
                x11-libs/gdk-pixbuf:2.0
                x11-libs/gtk+:2
                x11-libs/gtkglext
            )
    "
fi

if ! ever is_scm ; then
    WORK=${WORKBASE}/${PNV}-source
fi

ufoai_src_prepare() {
    if ! ever is_scm ; then
        edo mv "${WORKBASE}"/base "${WORK}"
        expatch "${FILES}"/${PNV}-make-LOCALEDIR-work.patch
        expatch "${FILES}"/${PNV}-fix_anon_namespace_for_CameraDrawMode.patch

        # Make sure to link libm in - not a patch because 2.4 is outdated.
        edo sed -i -e "/INTL_LIBS/s:\(?=\).*:\1 -lm:" build/default.mk
    else
        # Prevent stripping
        edo sed -i -e "/[Ss]trip/d" Makefile
        edo sed -i -e "/INSTALL_PROGRAM/s:-s::" Makefile
    fi

    append-flags -I/usr/include/lua5.1

    default
}

ufoai_src_configure() {
    local opts=()

    opts=(
        --target-os=linux

        --datadir=/usr/share/ufoai
        --libdir=/usr/${LIBDIR}/ufoai
        --localedir=/usr/share/locale
        --prefix=/usr

        --disable-testall # FIXME: tests need cunit
        --disable-memory
    )

    if ever at_least scm ; then
        opts+=(
            --enable-ufo2map
            --enable-ufomodel
            --enable-uforadiant
            --enable-ufoslicer
            --disable-sdl2
        )
    else
        opts+=(
            $(option_enable !debug release)
            $(option_enable editor ufo2map)
            $(option_enable editor ufomodel)
            $(option_enable editor uforadiant)
            $(option_enable editor ufoslicer)
        )
    fi

    if option platform:amd64; then
        opts+=( --enable-sse )
    fi

    # avoid an automagic dependency on mxml
    HAVE_MXML_H=no \
        edo ./configure "${opts[@]}"
}

ufoai_src_compile() {
    emake Q= all

    if ever is_scm ; then
        emake Q= maps
        emake Q= pk3
    fi

    emake -j1 Q= lang
}

ufoai_src_install() {
    emagicdocs

    # install scripts not respecting DESTDIR still exist in 2012... and 2014
    dobin ufo ufoded

    if ! ever is_scm ; then
        option editor && dobin ufo2map ufomodel radiant/uforadiant ufoslicer
    else
        dobin ufo2map ufomodel radiant/uforadiant ufoslicer
    fi

    exeinto /usr/${LIBDIR}/ufoai
    doexe base/game.so

    insinto /usr/share/locale
    doins -r base/i18n/*

    insinto /usr/share/ufoai/base
    doins base/*.pk3
}

