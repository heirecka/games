# Copyright 2009 Mike Kelly
# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=${PV/_/}

require freedesktop-desktop gtk-icon-cache

SUMMARY="Engine for running various old adventure games"
DESCRIPTION="
ScummVM is a program which allows you to run certain classic graphical
point-and-click adventure games, provided you already have their data
files. The clever part about this: ScummVM just replaces the executables
shipped with the games, allowing you to play them on systems for which
they were never designed!
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/frs/${PN}/${PV}/${PNV}.tar.xz"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/frs/${PN}/${PV}/ReleaseNotes [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation/ [[ lang = en ]]"

LICENCES="BSD-3 GPL-2 GPL-3 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    alsa
    debug [[ description = [ create a debug build ] ]]
    flac
    fluidsynth [[ description = [ Midi sound support by means of the fluidsynth engine ] ]]
    mp3
    nasm [[ description = [ assembly language optimizations ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        media-libs/faad2
        media-libs/freetype:2
        media-libs/libmpeg2[>=0.4.0]
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libtheora
        media-libs/libvorbis
        media-libs/SDL:2
        media-libs/SDL_net:2
        net-misc/curl
        alsa? ( sys-sound/alsa-lib )
        flac? ( media-libs/flac )
        fluidsynth? ( media-sound/fluidsynth )
        mp3? ( media-libs/libmad )
        nasm? ( dev-lang/nasm )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

WORK=${WORKBASE}/${PN}-${MY_PV}

_scumm_option() {
    local myoption=${1} mapping=${2}
    if option ${myoption} ; then
        if [[ -n ${mapping} ]]; then
            echo "--with-${mapping}-prefix=/usr"
        else
            echo "--with-${myoption}-prefix=/usr"
        fi
    else
        if [[ -n ${mapping} ]]; then
            echo "--disable-${mapping}"
        else
            echo "--disable-${myoption}"
        fi
    fi
}

src_prepare() {
    default

    edo sed \
        -e '/\$(INSTALL)/s/-s //' \
        -i ports.mk

    edo sed \
        -e "s:\(_strings\)=\(strings\):\1=$(exhost --tool-prefix)\2:" \
        -e "s:\(_ar\)=\"\(ar\):\1=\"$(exhost --tool-prefix)\2:" \
        -e "s:\(_ranlib\)=\(ranlib\):\1=$(exhost --tool-prefix)\2:" \
        -i configure
}

src_configure() {
    local myconf=(
        --host=$(exhost --build)
        --prefix=/usr
        --bindir=/usr/$(exhost --target)/bin
        --libdir=/usr/$(exhost --target)/lib
        --backend=sdl
        --default-dynamic
        --enable-all-engines
        --enable-cloud
        --enable-faad
        --enable-freetype2
        --enable-jpeg
        --enable-libcurl
        --enable-mpeg2
        --enable-ogg
        --enable-png
        --enable-plugins
        --enable-sdlnet
        --enable-theoradec
        --enable-verbose-build
        --enable-vorbis
        --disable-eventrecorder
        --disable-readline
        --disable-sndio
    )

    # Standard option handling
    myconf+=( $(_scumm_option alsa) )
    myconf+=( $(_scumm_option flac) )
    myconf+=( $(_scumm_option fluidsynth) )
    myconf+=( $(_scumm_option mp3 mad) )
    myconf+=( $(_scumm_option nasm) )

    # Special options
    option debug || myconf+=( --disable-debug --enable-release )
    option debug && myconf+=( --enable-debug --disable-release )

    # Custom configure script
    CXX="${CXX}" edo ./configure "${myconf[@]}"
}

src_install() {
    default

    edo rm -rf "${IMAGE}"/usr/share/doc/scummvm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

