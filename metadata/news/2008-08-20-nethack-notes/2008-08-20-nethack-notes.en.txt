Title: Notes on NetHack installation
Author: David Leverton <dleverton@exherbo.org>
Content-Type: text/plain
Posted: 2008-08-20
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: games-roguelike/nethack

NetHack in Exherbo is slotted according to save/bones file
compatibility, so you can finish old saved games even after installing
a new incompatible version.

If NetHack was built with the "xaw" option:

    A sample configuration file for the X11 interface has been
    installed at /usr/share/doc/nethack-*/nethack.rc.  You may wish to
    adapt it to your needs and save it as ~/.nethackrc.

    To enable tiles in the X11 interface, edit
    /etc/X11/app-defaults/NetHack and uncomment the
    "NetHack.tile_file: x11tiles" line, or set the resource using some
    other means.

    The NetHack fonts have been installed at /usr/lib*/nethack-*/fonts.
    Add them to /etc/X11/xorg.conf, or with "xset fp", if you want to
    use them.

